import * as Api from '../api';
export const SIGNIN_SUCCESS = 'auth/SIGNIN_SUCCESS';
export const SIGNIN_FAIL = 'auth/SIGNIN_FAIL';
export const SIGNUP_SUCCESS = 'auth/SIGNUP_SUCCESS';
export const SIGNUP_FAIL = 'auth/SIGNUP_FAIL';

export const login = data => dispatch => {
  return new Promise((resolve, reject) => {
    resolve(
      Api.login(data)
        .then(res => res.json())
        .then(data => {
					console.log(data);
					if(data.success){
						return dispatch({ type: SIGNIN_SUCCESS, payload: data });
					}
					else{
						return dispatch({ type: SIGNIN_FAIL, payload: data });
					}

        })
    );
  });
};

export const signup = data => dispatch => {
  return new Promise((resolve, reject) => {
    resolve(
      Api.signup(data)
        .then(res => res.json())
        .then(data => {
					console.log(data);
					if(data.success){
						return dispatch({ type: SIGNUP_SUCCESS, payload: data });
					}
					else{
						return dispatch({ type: SIGNUP_FAIL, payload: data });
					}

        })
    );
  });
};
