const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = mongoose.model('question', new Schema({
	question: {
		type: String,
		required: true
	},
	user:{
		type: Object,
		required: true,
		properties:{
			name:{
				type: String,
				required:true
			},
			email:{
				type: String,
				required: true
			}
		}

	}


 },{ versionKey: false }));

 module.exports = user;