const express = require('express');

var redis   = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);

var client  = redis.createClient();

const app = express();
const mongoose = require('mongoose');

const cors = require('cors');
const bodyParser = require('body-parser');
const userDetails = require('./schemas/userSchema');
const questionDetails = require('./schemas/questionShema');
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
// mongodb+srv://helpingStranger:<password>@cluster0-ynyv0.mongodb.net/test?retryWrites=true&w=majority

app.use(session({
  secret: 'ssshhhhh',
  // create new redis store.
  store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl : 260}),
  saveUninitialized: false,
  resave: false
}));

mongoose.connect(
  'mongodb+srv://@cluster0-ynyv0.mongodb.net/test?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    auth: {
      user: 'helpingStranger',
      password: 'helpingstranger123'
    }
  }
);

var db = mongoose.connection
  .once('open', function() {
    console.log('connection made');
  })
  .on('error', function(error) {
    console.log('connection error', error);
  });

app.post('/signup', (req, res, next) => {
  userDetails
    .find({ email: req.body.email })
    .then(data => {
      if (data.length === 0) {
        userDetails.insertMany(req.body, (err, data) => {
          if (err) {
            next(err);
          } else res.send({ success: 'registered successfully' });
        });
      } else {
        next({ status: 422, message: 'Email has already been taken.' });
      }
    })
    .catch(error => {
      next(error);
    });
});

app.post('/login', (req, res, next) => {
  console.log(req.body);
  req.session.key=req.body.email;

  userDetails
    .find({
      $and: [{ email: req.body.email }, { password: req.body.password }]
    })
    .then(data => {
      if (data.length === 1) {
        res.send({ success: data[0] });
      } else {
        next({ status: 422, message: 'Invalid email or password.' });
      }
    })
    .catch(error => next(error));
});

app.post('/question', (req,res,next) => {
  questionDetails.insertMany(req.body, (err, data) => {
    if (err) {
      next(err);
    } else res.send({ success: 'registered successfully' });
  });
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
