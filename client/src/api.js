const host = 'http://192.168.2.108:6600';

export const login = data => {
  return fetch(`${host}/login`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
};

export const signup = data => {
  return fetch(`${host}/signup`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
};
