import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import { details } from './details';
import Auth from '../Auth/reducer';

const reducers = {
  details,
	routing: routerReducer,
	form: formReducer,
	Auth
};

const sortedReducers = Object.keys(reducers).sort().reduce((acc, cur) => {
	acc[cur] = reducers[cur];
	return acc;
}, {});

export default sortedReducers;