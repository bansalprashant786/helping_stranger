import {
  SIGNIN_SUCCESS,
  SIGNIN_FAIL,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL
} from './action';

const initialState = {
  loaded: false,
  loading: false,
  user: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SIGNIN_SUCCESS:
      //todo: find a better way
      return {
        ...state,
        loggingIn: false,
				loaded: false,
				loggedIn: true,
        user: action.payload,
        error: null
      };

    case SIGNIN_FAIL:
      return {
        ...state,
        loggingIn: false,
        loggedIn: false,
        user: null,
        error: action.payload.error
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        signedUp: true,
        signingUp: false,
        loaded: false
      };
    case SIGNUP_FAIL:
      return {
        ...state,
        signingUp: false,
        signedUp: false,
        signUpError: action.payload.errors || action.payload
      };

    default:
      return state;
  }
}


export function isSignedUp(globalState) {
  return (
    (globalState.auth &&
      globalState.auth.signedUp) ||
    false
  );
}

export function isLogin(globalState) {
	return (
    (globalState.auth &&
      globalState.auth.loggedIn) ||
    false
  );
}

