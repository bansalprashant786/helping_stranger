export const required = value => (value ? undefined : 'required');

export const email = (value) => {
  if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,8}$/i.test(value)) {
    return 'Invalid Email';
  }
  return undefined;
};
export const matchFields = (validateFields = []) => (value, fields) => (fields[validateFields[0]] === fields[validateFields[1]] ? undefined : 'Passwords did not match');

export const matchPasswordFields = matchFields(['password', 'password_confirmation']);

export const phoneNumber = (value) => {
  if (!value) {
    return undefined;
  }
  if ((`${value}` === `${parseInt(value, 10)}`) &&
    value.length >= 5 &&
    value.length <= 15
  ) {
    return undefined;
  }
  return 'Phone number not valid';
};
