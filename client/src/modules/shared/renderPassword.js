import React from 'react';

const RenderPassword = ({
  input,
  type,
  handleClick,
  showEye,
  placeholder,
  meta: { touched, error }
}) => (
  <div>
    <div>
      <span className='input-item'>
        <i className='fa fa-key' />
      </span>
      <input
        {...input}
        className='form-input'
        type={type}
        id='pwd'
        placeholder={placeholder}
      />
      {showEye ? (
        <span onClick={() => handleClick()}>
          <i className='fa fa-eye' aria-hidden='true' type='button' id='eye' />
        </span>
      ) : null}
    </div>
    {touched && error && (
      <span className='relative input-error-text'>{error}</span>
    )}
  </div>
);

export default RenderPassword;
