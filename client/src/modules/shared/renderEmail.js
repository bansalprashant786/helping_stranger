import React from 'react';

const RenderEmail = ({ input, meta: { touched, error } }) => (
  <div>
    <div>
      <span className='input-item'>
        <i className='fa fa-envelope-square' />
      </span>
      <input
        {...input}
        className='form-input'
        id='txt-input'
        name='email'
        type='text'
        placeholder='@UserName'
      />
    </div>
    {touched && error && (
      <span className="relative input-error-text" >{error}</span>
    )}
  </div>
);

export default RenderEmail;
