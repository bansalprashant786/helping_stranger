import { asyncConnect } from 'redux-connect';
import Loadable from 'react-loadable';

import { inTheatres } from '../../action';

const HomeAsync = Loadable({
  loader: () => import(/* webpackChunkName: 'Home' */ './HomeAsync'),
  loading: () => null
});

export default asyncConnect(
  [
    { promise: () => import('./HomeAsync') },
  ],
  state => ({
		theatresData : state.details.inTheatres
  }),
  { inTheatres }
)(HomeAsync);