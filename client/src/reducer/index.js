import { createStore , applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import reducers from './Reducer'

const configureStore = (history) => {

	const middleware = [
    routerMiddleware(history),
    thunk
	];

	const finalCreateStore = compose(
		applyMiddleware(...middleware),
		// window.devToolsExtension && window.devToolsExtension(),
)(createStore);
	return finalCreateStore(combineReducers(reducers));
}

export const store = configureStore(browserHistory);
