import React from 'react';

const RenderPassword = ({ input, type, meta: { touched, error } }) => (
  <div>
    <div>
      <span className='input-item'>
        <i className='fa fa-key' />
      </span>
      <input
        {...input}
        className='form-input'
        type={type}
        id='pwd'
        placeholder='Confirm Password'
      />
      <span onClick={() => handleClick()}>
        <i className='fa fa-eye' aria-hidden='true' type='button' id='eye' />
      </span>
    </div>
    {touched && error && (
      <span className='relative input-error-text'>{error}</span>
    )}
  </div>
);

export default RenderPassword;
