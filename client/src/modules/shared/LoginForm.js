import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';

import Login from './login';
import SignUp from './signUp';
import { login, signup } from '../../Auth/action';

class LoginForm extends Component {
  handleLogin = values => {
		return this.props.login(values)
		.then(
      (data) => {
        if(data.payload.success){
				}
				else if(data.payload.error){
					throw new SubmissionError({ password:data.payload.error.message });
				}
      }
    ).catch(
      (error) => {
        if(error.errors) {
          if(error.errors.password) {
            throw new SubmissionError({ password: error.errors.password });
          } else {
            throw new SubmissionError({ _error: 'Something went wrong. Please try later' });
          }
        } else {
          if(!error.status){
            throw new SubmissionError({ password: 'No Internet Connection.' });
          }
        }
      }
    );
  };

  handleSignUp = values => {
		return this.props.signup(values)
		.then(
      (data) => {
        if(data.payload.success){
				}
				else if(data.payload.error){
					throw new SubmissionError({ email:data.payload.error.message });
				}
      }
    ).catch(
      (error) => {
        if(error.errors) {
          if(error.errors.email) {
            throw new SubmissionError({ email: error.errors.email });
          } else {
            throw new SubmissionError({ _error: 'Something went wrong. Please try later' });
          }
        } else {
          if(!error.status){
            throw new SubmissionError({ email: 'No Internet Connection.' });
          }
        }
      }
    );

	};

  render() {
    return (
      <div>

        {this.props.route.type === 'login' ? (
          <Login onSubmit={this.handleLogin} />
        ) : (
          <SignUp onSubmit={this.handleSignUp}/>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
	// isLogged
});

const mapDispatchToProps = dispatch => ({
	login: params => dispatch(login(params)),
  signup: params => dispatch(signup(params)),

});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
