import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import { store } from './reducer';
import { Router } from 'react-router';

import routes from './routes';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';


const history = syncHistoryWithStore(browserHistory, store);

const onUpdate = () => {
  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0);
  }
};

ReactDOM.render(
<Provider store={store}>
	<Router
		history={history}
		onUpdate={onUpdate}

	>
		{routes(store)}

	</Router>
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
