import React, { Component } from 'react';
import './login.css';
import { Field, reduxForm } from 'redux-form';
import { required, email } from '../../helpers/formValidator';
import { Link } from 'react-router';

import RenderEmail from '../renderEmail';
import RenderPassword from '../renderPassword';

class Login extends Component {
  state = {
    show: false
  };

  handleClick = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    return (
      <div>
        <div className='overlay'>
          <form className='form-start' onSubmit={this.props.handleSubmit}>
            <div className='con'>
              <header className='head-form'>
                <h2>Log In</h2>
                <p>login here using your username and password</p>
              </header>
              <div className='field-set'>
                <div>
                  <Field
                    name='email'
                    component={RenderEmail}
                    validate={[required, email]}
                  />
                </div>
                <div>
                  <Field
                    name='password'
                    placeholder='Password'
                    showEye={true}
                    component={RenderPassword}
                    validate={[required]}
                    handleClick={this.handleClick}
                    type={`${this.state.show ? 'text' : 'password'}`}
                  />
                </div>
                <button className='log-in button'> Log In </button>
              </div>

              <div className='other'>
                <Link to='' className='btn submits frgt-pass button' >
                  Forgot Password
                </Link>
                <Link to = '/sign_up'className='btn submits sign-up button'>
                  Sign Up
                  <i className='fa fa-user-plus' aria-hidden='true' />
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default reduxForm({
  form: 'signIn-A709E'
})(Login);
