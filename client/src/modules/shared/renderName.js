import React from 'react';

const RenderName = ({ input, meta: { touched, error } }) => (
  <div>
    <div>
      <span className='input-item'>
        <i className='fa fa-user-circle' />
      </span>
      <input
        {...input}
        className='form-input'
        id='txt-input'
        name='name'
        type='text'
        placeholder='Full name'
      />
    </div>
    {touched && error && (
      <span className="relative input-error-text" >{error}</span>
    )}
  </div>
);

export default RenderName;
