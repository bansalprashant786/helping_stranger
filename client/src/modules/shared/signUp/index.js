import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router';

import { required, email, matchPasswordFields, phoneNumber } from '../../helpers/formValidator';
import RenderEmail from '../renderEmail';
import RenderPassword from '../renderPassword';
import RenderPhone from '../renderPhone';
import RenderName from '../renderName';


class Signup extends Component {
  state = {
    show: false
  };

  handleClick = () => {
    this.setState({ show: !this.state.show });
	};

	render(){
		return(
			<div>
				<div className='overlay'>
          <form className='form-start' onSubmit={this.props.handleSubmit}>
            <div className='con'>
              <header className='head-form'>
                <h2>Sign Up</h2>
                <p>Sign up here for new experience</p>
              </header>
              <div className='field-set'>
								<div>
                  <Field
                    name='name'
                    component={RenderName}
                    validate={[required]}
                  />
                </div>
                <div>
                  <Field
                    name='email'
                    component={RenderEmail}
                    validate={[required, email]}
                  />
                </div>
                <div>
                  <Field
										name='password'
										placeholder='Password'
                    component={RenderPassword}
                    validate={[required]}
                    type='password'
                  />
                </div>
								<div>
                  <Field
										name='password_confirmation'
										showEye={true}
										placeholder='Confirm Password'
                    component={RenderPassword}
                    validate={[required, matchPasswordFields]}
                    handleClick={this.handleClick}
                    type={`${this.state.show ? 'text' : 'password'}`}
                  />
                </div>
								<div>
                  <Field
                    name='phone_number'
                    component={RenderPhone}
                    validate={[required, phoneNumber]}
                  />
                </div>

                <button className='log-in button'> Sign Up </button>
              </div>

              <div className='other'>
                <div className='' >
                  Already have an Account?
                <Link to = '/sign_in'className='btn ml14 '>
                  Login
                </Link>

								</div>

              </div>
            </div>
          </form>
        </div>
			</div>
		);
	};
};

export default reduxForm({
  form: 'signUp-2SBE5',
})(Signup);
