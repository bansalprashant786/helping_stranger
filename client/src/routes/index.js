import React from 'react';
import { IndexRoute, Route, Redirect } from 'react-router';
import Home from '../screen/home';
import App from '../App';
import Footer from '../modules/shared/Footer';
import Error404 from '../modules/shared/Error404';
import LoginForm from '../modules/shared/LoginForm';
const checkAuthenticated = (nextState, replace, cb) => {
	const checkAndRedirect = () => {
		if (nextState.location.pathname !== '/home' ) {
			replace('/users/sign_in');
		}
		cb();
	};
	checkAndRedirect();
};
const routes = () => {

	return(
		<Route path="/" component={App}>
			<IndexRoute component={Home}navSticky />
			<Route path='sign_in' component ={LoginForm}type='login' />
			<Route path='sign_up' component ={LoginForm}type='signup' />

			<Route onEnter={checkAuthenticated}>
			<Route path='/home' component={Home} />
				<Route path="/footer" component={Footer} />
			</Route>
			<Route path='/page-not-found' component={Error404} />
			<Route>
        <Redirect from='*' to={'/page-not-found'} topBarOnly hideChatBot />
      </Route>
		</Route>
	);

};

export default routes;
