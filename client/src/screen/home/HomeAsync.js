import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Header from '../../modules/shared/Header';

class HomeAsync extends Component {

	componentDidMount(){
		this.props.inTheatres();
	}

	render(){
		return(
			<div>
				<Header/>
			</div>
		);
	}
};

export default HomeAsync;

HomeAsync.propTypes = {
	theatresData: PropTypes.array,
	inTheatres: PropTypes.func
}