const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = mongoose.model('user_details', new Schema({
     password:{
				type:String,
				required:true
		 },
     name:{
         type:String,
         required:true
     },
     imageUrl:{
         type:String,
        //  required:true
     },
     email:{
         type:String,
         required:true
     },
     phone_number:{
        type:Number,
        required:true
     },
 },{ versionKey: false }));

 module.exports = user;